stages:
  - test
  - staging
  - production

test:
  stage: test
  script:
    - apt-get update -y
    - apt-get install -y python-dev python3-pip
    - cd ./app
    - pip3 install --no-cache-dir -r requirements.txt
    - python3 test.py

staging:
  stage: staging
  script:
    - echo "Deploying to the staging server"
    - cp staging.config.py config.py
  environment:
    name: staging
    url: https://examplelocation.com
  only:
    - master

production:
  stage: production
  script:
    - echo "Deploying to the production server"
    - cp production.config.py config.py
  environment:
    name: production
    url: https://examplelocation.com
  when:
    - manual
  only:
    - master
