import os
import sys
import unittest

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import app
import json
from http import HTTPStatus

class TestAPIMethods(unittest.TestCase):
    # Empty for now
    pass

# On the same indentation level as the class, as this code does not belong to the class
if __name__ == '__main__':
    unittest.main()
